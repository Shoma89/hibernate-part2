package HibernateHW.Test;

import HibernateHW.Model.Student;
import HibernateHW.Model.Teacher;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class TestUpdate {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Teacher.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Student student = em.find(Student.class, 8L);
        if(student != null){
            student.setName("Sergey");
            student.setLastName("Lovrov");
            student.setCourse(2);
        } else{
            System.out.println("Not found!");
        }
        em.getTransaction().commit();
    }
}
