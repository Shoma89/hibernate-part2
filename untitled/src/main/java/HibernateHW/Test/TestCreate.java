package HibernateHW.Test;

import HibernateHW.Model.Student;
import HibernateHW.Model.Teacher;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class TestCreate {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Teacher.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Teacher teacher1 = Teacher.builder()
                .name("Aleksandr")
                .lastName("Pushkin")
                .discipline("Literature").build();
        Teacher teacher2 = Teacher.builder()
                .name("Dmitriy")
                .lastName("Mendeelev")
                .discipline("Chemistry").build();
        Teacher teacher3 = Teacher.builder()
                .name("Igor")
                .lastName("Kurchatov")
                .discipline("Physics").build();
        Teacher teacher4 = Teacher.builder()
                .name("Ilon")
                .lastName("Mask")
                .discipline("Informatics").build();
        Student student1 = Student.builder()
                .name("Ivan")
                .lastName("Ivanov")
                .course(1).build();
        Student student2 = Student.builder()
                .name("Petr")
                .lastName("Solovyov")
                .course(1).build();
        Student student3 = Student.builder()
                .name("Natalia")
                .lastName("Sergeeva")
                .course(2).build();
        Student student4 = Student.builder()
                .name("Victoria")
                .lastName("Andreeva")
                .course(5).build();
        Student student5 = Student.builder()
                .name("Ekaterina")
                .lastName("Belova")
                .course(2).build();
        Student student6 = Student.builder()
                .name("Vlad")
                .lastName("Izmailov")
                .course(6).build();

        teacher1.addStudent(student1);
        teacher1.addStudent(student2);
        teacher1.addStudent(student3);
        teacher1.addStudent(student5);

        teacher2.addStudent(student1);
        teacher2.addStudent(student6);
        teacher2.addStudent(student4);

        teacher3.addStudent(student1);
        teacher3.addStudent(student6);

        teacher4.addStudent(student1);
        teacher4.addStudent(student2);
        teacher4.addStudent(student3);
        teacher4.addStudent(student4);
        teacher4.addStudent(student5);
        teacher4.addStudent(student6);

        em.persist(teacher1);
        em.persist(teacher2);
        em.persist(teacher3);
        em.persist(teacher4);

        em.persist(student1);
        em.persist(student2);
        em.persist(student3);
        em.persist(student4);
        em.persist(student5);
        em.persist(student6);

        em.getTransaction().commit();
    }
}
