package HibernateHW.Test;

import HibernateHW.Model.Student;
import HibernateHW.Model.Teacher;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class TestDelete {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Teacher.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Student student = em.find(Student.class, 1L);
        if(student != null){
            em.remove(student);
            System.out.println("Student remove!");
        } else{
            System.out.println("Not Found!");
        }
        em.getTransaction().commit();
    }
}
