package HibernateHW.Test;

import HibernateHW.Model.Student;
import HibernateHW.Model.Teacher;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class TestRead {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Teacher.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        Student student = em.find(Student.class, 10L);
        Teacher teacher = em.find(Teacher.class, 5L);
        System.out.println(student);
        System.out.println(student.getTeacherList());
        System.out.println(teacher);
        System.out.println(teacher.getStudentList());
        em.getTransaction().commit();
    }
}
