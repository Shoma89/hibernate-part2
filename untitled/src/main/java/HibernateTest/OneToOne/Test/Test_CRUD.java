package HibernateTest.OneToOne.Test;

import HibernateTest.OneToOne.Entity.Details;
import HibernateTest.OneToOne.Entity.Employee;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class Test_CRUD {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Details.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        // добавление в таблицу объекта
//        em.getTransaction().begin();
//        Employee employee = Employee.builder().name("Vladimir").lastName("Lenin").department("Creative Director").salary(16459.1).build();
//        em.persist(employee);
//        em.getTransaction().commit();

        // поиск объекта в таблице
//        em.getTransaction().begin();
//        Employee employee = em.find(Employee.class, 7L);
//        em.getTransaction().commit();

        //изменение объекта в таблице
//        em.getTransaction().begin();
//        employee.setDepartment("Administration");
//        employee.setSalary(20000);
//        em.getTransaction().commit();

        //удаление объекта из таблицы
//        em.getTransaction().begin();
//        if(employee != null) {
//            em.remove(employee);
//        } else {
//            System.out.println("Объект не найден!");
//        }
//        em.getTransaction().commit();

        //поиск в таблице сущности по определенным условиям
        em.getTransaction().begin();
        Details details = em.find(Details.class, 3L);
        List<Employee> list = em.createQuery("from EmployeeEntity where details = " + details.getId()).getResultList();
        System.out.println(list);
        em.getTransaction().commit();
    }
}
