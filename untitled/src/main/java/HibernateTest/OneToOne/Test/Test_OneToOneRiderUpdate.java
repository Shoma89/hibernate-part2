package HibernateTest.OneToOne.Test;

import HibernateTest.OneToOne.Entity.Details;
import HibernateTest.OneToOne.Entity.Employee;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Test_OneToOneRiderUpdate {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Details.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Employee employee = em.find(Employee.class, 2L);
        Details details = employee.getDetails();
        employee.setSalary(20000);
        details.setMail("Mask@gmail.com");
        details.setPhone("14895637014");
        System.out.println(employee);
        System.out.println(details);
        em.getTransaction().commit();
    }
}
