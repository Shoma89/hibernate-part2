package HibernateTest.OneToOne.Test;

import HibernateTest.OneToOne.Entity.Details;
import HibernateTest.OneToOne.Entity.Employee;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Test_OneToOneDelete {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Details.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Employee employee = em.find(Employee.class, 5L);
        if(employee != null){
            em.remove(employee);
            System.out.println("Работник удалён!");
        } else {
            System.out.println("Работник не найден!");
        }
        em.getTransaction().commit();
    }
}
