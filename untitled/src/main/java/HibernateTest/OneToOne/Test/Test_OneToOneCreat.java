package HibernateTest.OneToOne.Test;

import HibernateTest.OneToOne.Entity.Details;
import HibernateTest.OneToOne.Entity.Employee;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Test_OneToOneCreat {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Details.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Employee employee = Employee.builder()
                .name("Aleksandr")
                .lastName("Pushkin")
                .department("IT")
                .salary(16002)
                .details(Details.builder()
                        .city("Sankt-Peterburg")
                        .phone("89375126487")
                        .mail("Pushkin@gmail.com")
                        .build())
                .build();
        em.persist(employee);
        em.getTransaction().commit();
    }
}
