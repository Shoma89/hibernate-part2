package HibernateTest.OneToMany.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Departments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nameDepartment;
    private Double maxSalary;
    private Double minSalary;
    @OneToMany(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH},
               mappedBy = "department")
    private List<EmployeeEntity> employeeEntityList;

    public void addEmployeeToDepartment(EmployeeEntity employeeEntity){
        if(employeeEntityList == null){
            employeeEntityList = new ArrayList<>();
        }
        employeeEntityList.add(employeeEntity);
        employeeEntity.setDepartment(this);
    }

    @Override
    public String toString() {
        return "Departments{" +
                "id=" + id +
                ", nameDepartment='" + nameDepartment + '\'' +
                ", maxSalary=" + maxSalary +
                ", minSalary=" + minSalary +
                '}';
    }
}
