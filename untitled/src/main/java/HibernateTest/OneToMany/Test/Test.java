package HibernateTest.OneToMany.Test;

import HibernateTest.OneToMany.Entity.Departments;
import HibernateTest.OneToMany.Entity.EmployeeEntity;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Test {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(EmployeeEntity.class)
                .addAnnotatedClass(Departments.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();

        em.getTransaction().begin();
        Departments departments = Departments.builder()
                .nameDepartment("IT")
                .minSalary(3000.00)
                .maxSalary(10000.00)
                .build();
        EmployeeEntity employee = EmployeeEntity.builder()
                .name("Vladimir")
                .lastName("Putin")
                .salary(10000.00)
                .department(departments)
                .build();
        EmployeeEntity employee1 = EmployeeEntity.builder()
                .name("Sergey")
                .lastName("Lavrov")
                .salary(7000.00)
                .build();
        departments.addEmployeeToDepartment(employee);
        departments.addEmployeeToDepartment(employee1);
        EmployeeEntity employee2 = em.find(EmployeeEntity.class, 1l);
        System.out.println(employee2.getDepartment());
        em.getTransaction().commit();
    }
}
