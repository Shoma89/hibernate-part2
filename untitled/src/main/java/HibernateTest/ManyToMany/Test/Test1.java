package HibernateTest.ManyToMany.Test;

import HibernateTest.ManyToMany.Entity.Children;
import HibernateTest.ManyToMany.Entity.Section;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Test1 {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Children.class)
                .addAnnotatedClass(Section.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();

        em.getTransaction().begin();
//        Section section = Section.builder().name("Football").build();
//        Section section1 = Section.builder().name("Shahmatyi").build();
//        Section section2 = Section.builder().name("Plavanie").build();
//        Children children = Children.builder().name("Igor").lastName("Ivanov").age(6).build();
//        Children children1 = Children.builder().name("Oleg").lastName("Sidarov").age(8).build();
//        Children children2 = Children.builder().name("Aleksandr").lastName("Petrov").age(9).build();
//        Children children3 = Children.builder().name("Victoriya").lastName("Sergeeva").age(7).build();
//        Children children4 = Children.builder().name("Pelageya").lastName("Soloviova").age(5).build();
//
//        section.addChildrenList(children);
//        section.addChildrenList(children2);
//        section.addChildrenList(children4);
//
//        section1.addChildrenList(children3);
//        section1.addChildrenList(children);
//        section1.addChildrenList(children1);
//
//        section2.addChildrenList(children1);
//        section2.addChildrenList(children4);
//        section2.addChildrenList(children2);

//        em.persist(section);
//        em.persist(section1);
//        em.persist(section2);
//
//        em.persist(children);
//        em.persist(children1);
//        em.persist(children2);
//        em.persist(children3);
//        em.persist(children4);
        Children children = em.find(Children.class, 2L);
        if(children != null){
            children.setAge(15);
            System.out.println("изменён!");

        } else{
            System.out.println("не найден!");
        }

        em.getTransaction().commit();

    }
}
