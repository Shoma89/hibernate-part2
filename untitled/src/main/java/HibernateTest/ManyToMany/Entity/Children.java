package HibernateTest.ManyToMany.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Children {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String lastName;
    private Integer age;
    @ManyToMany
    @JoinTable(name = "children_sections",
    joinColumns = @JoinColumn(name = "children_id"),
    inverseJoinColumns = @JoinColumn(name = "section_id"))
    private List<Section> sectionList;

    public void addSectionList(Section section){
        if(sectionList == null){
            sectionList = new ArrayList<>();
        }
        sectionList.add(section);
    }

    @Override
    public String toString() {
        return "Children{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
